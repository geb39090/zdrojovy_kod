#include "stm8s.h"

void delay_init(void)
{
    TIM4_TimeBaseInit(TIM4_PRESCALER_64, 249);
    TIM4_Cmd(ENABLE);
}

void delay_1s(void)
{
    TIM4_SetCounter(0);
    for(int ms = 0; ms < 1000; ms++)
    {
        while(TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET);
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
    }
}

void delay_01s(void)
{
    TIM4_SetCounter(0);
    for(int ms = 0; ms < 100; ms++)
    {
        while(TIM4_GetFlagStatus(TIM4_FLAG_UPDATE) != SET);
        TIM4_ClearFlag(TIM4_FLAG_UPDATE);
    }
}

void init_segment1(void)
{
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOE, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
}

void off_segment1(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    GPIO_WriteLow(GPIOG, GPIO_PIN_5);
    GPIO_WriteLow(GPIOG, GPIO_PIN_6);
    GPIO_WriteLow(GPIOE, GPIO_PIN_7);
    GPIO_WriteLow(GPIOE, GPIO_PIN_6);
    GPIO_WriteLow(GPIOG, GPIO_PIN_7);
    GPIO_WriteLow(GPIOB, GPIO_PIN_5);
}
void show_segment1(int number)
{
    if (number == 0)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 1)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);

    }
    else if (number == 2)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 3)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 4)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
    }
    else if (number == 5)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 6)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 7)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
    }
    else if (number == 8)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
    else if (number == 9)
    {
        off_segment1();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_5);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOE, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_5);
    }
}


void init_segment2(void)
{
    GPIO_Init(GPIOA, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOA, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOB, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_SLOW);
}

void off_segment2(void)
{
    GPIO_WriteLow(GPIOA, GPIO_PIN_6);
    GPIO_WriteLow(GPIOA, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_4);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
    GPIO_WriteLow(GPIOB, GPIO_PIN_2);
    GPIO_WriteLow(GPIOB, GPIO_PIN_1);
    GPIO_WriteLow(GPIOB, GPIO_PIN_0);
    GPIO_WriteLow(GPIOB, GPIO_PIN_3);
}

void show_segment2(int number)
{
    if (number == 0)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
    }
    else if (number == 1)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);

    }
    else if (number == 2)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
    }
    else if (number == 3)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
    }
    else if (number == 4)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
    }
    else if (number == 5)
    {
        off_segment2();
        GPIO_WriteHigh(GPIOA, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOA, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_1);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_0);
        GPIO_WriteHigh(GPIOB, GPIO_PIN_3);
    }
}

void init_segment3(void)
{
    GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOG, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOD, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
}

void off_segment3(void)
{
    GPIO_WriteLow(GPIOG, GPIO_PIN_2);
    GPIO_WriteLow(GPIOG, GPIO_PIN_3);
    GPIO_WriteLow(GPIOD, GPIO_PIN_3);
    GPIO_WriteLow(GPIOC, GPIO_PIN_4);
    GPIO_WriteLow(GPIOC, GPIO_PIN_6);
    GPIO_WriteLow(GPIOC, GPIO_PIN_7);
    GPIO_WriteLow(GPIOC, GPIO_PIN_5);
}

void show_segment3(int number)
{
    if (number == 0)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
    }
    else if (number == 1)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);

    }
    else if (number == 2)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 3)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 4)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 5)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 6)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 7)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
    }
    else if (number == 8)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_2);
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
    else if (number == 9)
    {
        off_segment3();
        GPIO_WriteHigh(GPIOG, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOD, GPIO_PIN_3);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_4);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_6);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_7);
        GPIO_WriteHigh(GPIOC, GPIO_PIN_5);
    }
}

int counter = 0;
INTERRUPT_HANDLER(EXTI_PORTD_IRQHandler, 6)
{
    counter++;                                 // counter +1
    if (counter == 539) counter = 0;           // if counter == max diplay value -> reset counter
}

int choosen = 0;                               // chossen == 0 -> user did not choose time
INTERRUPT_HANDLER(EXTI_PORTE_IRQHandler, 7)
{
    choosen = 1;                               // chosen -> 1 -> user confimed value
}

void main(void){
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);                    // init led on STM
    GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_IN_FL_IT);                           // init button input
    GPIO_Init(GPIOE, GPIO_PIN_0, GPIO_MODE_IN_FL_IT);                           // init button input

    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOD,EXTI_SENSITIVITY_RISE_ONLY);      // setup interrupts
    EXTI_SetExtIntSensitivity(EXTI_PORT_GPIOE,EXTI_SENSITIVITY_RISE_ONLY);      // setup interrupts
    enableInterrupts();

    // init needed stuff
    delay_init();
    init_segment1();
    init_segment2();
    init_segment3();

    // show zeros at the beginning
    show_segment1(0);
    show_segment2(0);
    show_segment3(0);

    while (1)
    {
        // show counter value on 7segments
        show_segment1(counter % 10);            // seconds units
        show_segment2((counter % 60 )/ 10);     // seconds dozents
        show_segment3(counter / 60);            // minutes units

        // if user confirmed counter value do this while
        while (choosen == 1) {
            // calculate counter value to show on 7segments
            int counter_dozents = (counter % 60 )/ 10;
            int counter_units = counter % 10;
            int counter_mins_units = counter / 60;

            // show zeros at the beginning
            show_segment1(0);
            show_segment2(0);
            show_segment3(0);

            // minutes for loop
            for (int l = 0; ((l < 10) && (choosen == 1)); l++) {
                show_segment3(l);           // show minutes

                // seconds dozents for loop
                for (int j = 0; ((j < 6) && (choosen == 1)); j++) {
                    show_segment2(j);       // show seconds dozens

                    // seconds units for loop
                    for (int i = 0; ((i < 10) && (choosen == 1)); i++) {
                        // if showed minutes, seconds dozens and seconds units are the same as the user set -> blink value nad exit counting
                        if ((j == counter_dozents) && ((i == counter_units + 1) || (i == 9)) && (l == counter_mins_units)) {
                            // blink 10 times
                            for (int k = 0; k < 10; k++) {
                                off_segment1();
                                off_segment2();
                                off_segment3();
                                delay_01s();
                                show_segment1(counter_units);
                                show_segment2(counter_dozents);
                                show_segment3(counter_mins_units);
                                delay_01s();
                            }

                            // set choosen to 0, counter to 0 and break to exit counting in for loops
                            choosen = 0;
                            counter = 0;
                            break;
                        }
                        show_segment1(i);   // show seconds units
                        delay_1s();                // delay 1 s to time function
                    }
                }
            }
        }
    }
}
